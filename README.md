# Wandertag App Backend Helm chart

![Version: 1.2.1](https://img.shields.io/badge/Version-1.2.1-informational?style=flat-square)

This repository contains a [Helm](https://helm.sh/) chart to deploy the [Wandertag App Backend](https://github.com/mathezirkel-augsburg/wandertag-app) on Kubernetes.

The chart requires [Traefik v2](https://traefik.io/traefik/) with its associated CRDs.

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Felix Stärk | <felix.staerk@posteo.de> |  |
| Jonas Kell | <jonas-kell@web.de> |  |

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://gitlab.com/api/v4/projects/50287648/packages/helm/main/ | postgresql | 1.0.0 |

## Usage

**Make sure that the ingress is disabled when the app is not in use!**

## Installation and configuration

1. Create namespace.

    ```shell
    kubectl create namespace wandertag-app
    ```

2. Create a secret for the database crendentials:

    ```shell
    kubectl create secret generic database-credentials \
    --from-literal=user=postgres \
    --from-literal=password=CHANGEME \
    -n wandertag-app
    ```

3. Create a data secret:

    ```shell
    kubectl create secret generic wandertag-app-credentials \
    --from-literal=data-secret=CHANGEME \
    -n wandertag-app
    ```

4. Deploy the helm chart:

   ```shell
   helm upgrade --install -n wandertag-app wandertag-app-backend .
   ```

## Values

### Ingress

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| ingress.additionalMiddlewares | list | `[{"name":"security-headers","namespace":"kube-public"}]` | List of additional middlewares to apply on the ingress |

### Other Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| config.host | string | `"0.0.0.0"` | The webserver interface to listen on |
| config.port | int | `8888` | The containerPort of the webserver |
| config.uploadActive | bool | `false` | Enables the upload of data points |
| dataSecret.existingSecret.key | string | `"data-secret"` |  |
| dataSecret.existingSecret.name | string | `"wandertag-app-credentials"` |  |
| fullnameOverride | string | `""` | String to fully override wandertag-app.fullname |
| image.pullPolicy | string | `"Always"` | Wandertag App Backend image pull policy |
| image.repository | string | `"registry.gitlab.com/mathezirkel/content/wandertag-app"` | Wandertag App Backend image repository |
| image.tag | string | `"main"` | Wandertag App Backend image tag |
| ingress.enabled | bool | `true` | Enable the ingress |
| ingress.entryPoints | list | `["websecure"]` | List of traefik entrypoints to listen |
| ingress.host | string | `"wandertag-app-backend.mathezirkel-augsburg.de"` | Hostname the Wandertag App Backend should be available on |
| ingress.tls.certResolver | string | `"lets-encrypt"` | Name of the certResolver configured via traefik |
| livenessProbe.enabled | bool | `true` | Enable livenessProbe on Wandertag App Backend containers |
| livenessProbe.failureThreshold | int | `3` | Failure threshold for livenessProbe |
| livenessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for livenessProbe |
| livenessProbe.periodSeconds | int | `10` | Period seconds for livenessProbe |
| livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| livenessProbe.timeoutSeconds | int | `5` | Timeout seconds for livenessProbe |
| nameOverride | string | `""` | String to partially override wandertag-app.fullname |
| postgresql.existingSecret.name | string | `"database-credentials"` | Name of an existing secret containing the admin user credentials |
| postgresql.existingSecret.passwordKey | string | `"password"` | Key containing the admin user password |
| postgresql.fullnameOverride | string | `""` | String to fully override postgresql.fullname |
| postgresql.image.pullPolicy | string | `"IfNotPresent"` | PostgreSQL image pull policy |
| postgresql.image.repository | string | `"postgres"` | PostgreSQL image repository |
| postgresql.image.tag | string | postgresql.Chart.appVersion | PostgreSQL image tag |
| postgresql.livenessProbe.enabled | bool | `true` | Enable livenessProbe on PostgreSQL containers |
| postgresql.livenessProbe.failureThreshold | int | `6` | Failure threshold for livenessProbe |
| postgresql.livenessProbe.initialDelaySeconds | int | `30` | Initial delay seconds for livenessProbe |
| postgresql.livenessProbe.periodSeconds | int | `10` | Period seconds for livenessProbe |
| postgresql.livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| postgresql.livenessProbe.timeoutSeconds | int | `5` | Timeout seconds for livenessProbe |
| postgresql.nameOverride | string | `""` | String to partially override postgresql.fullname |
| postgresql.persistence.accessModes | list | `["ReadWriteOnce"]` | PVC Access Mode for PostgreSQL volume |
| postgresql.persistence.mountPath | string | `"/var/lib/postgresql/data"` | The path the volume will be mounted inside the container |
| postgresql.persistence.size | string | `"10Gi"` | PVC Storage Request for PostgreSQL volume |
| postgresql.persistence.storageClassName | string | `"retain-local-path"` | PVC StorageClass for PostgreSQL data volume |
| postgresql.persistence.subPath | string | `""` | The subdirectory of the volume to mount |
| postgresql.podManagementPolicy | string | `"OrderedReady"` | Pod management policy for the PostgreSQL statefulset |
| postgresql.readinessProbe.enabled | bool | `true` | Enable readinessProbe on PostgreSQL containers |
| postgresql.readinessProbe.failureThreshold | int | `6` | Failure threshold for readinessProbe |
| postgresql.readinessProbe.initialDelaySeconds | int | `5` | Initial delay seconds for readinessProbe |
| postgresql.readinessProbe.periodSeconds | int | `10` | Period seconds for readinessProbe |
| postgresql.readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| postgresql.readinessProbe.timeoutSeconds | int | `5` | Timeout seconds for readinessProbe |
| postgresql.replicaCount | int | `1` | Number of PostgreSQL replicas to deploy |
| postgresql.resources.limits.memory | string | `"512Mi"` | The memory limits for the PostgreSQL containers |
| postgresql.resources.requests.cpu | string | `"100m"` | The requested cpu for the PostgreSQL containers |
| postgresql.resources.requests.memory | string | `"256Mi"` | The requested memory for the PostgreSQL containers |
| postgresql.service.port | int | `5432` | PostgreSQL service port |
| postgresql.serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for PostgreSQL pod If set to false, the default ServiceAccount is used. |
| postgresql.serviceAccount.name | string | postgresql.fullname | The name of the service account to use. |
| postgresql.startupProbe.enabled | bool | `true` | Enable startupProbe on PostgreSQL containers |
| postgresql.startupProbe.failureThreshold | int | `15` | Failure threshold for startupProbe |
| postgresql.startupProbe.initialDelaySeconds | int | `30` | Initial delay seconds for startupProbe |
| postgresql.startupProbe.periodSeconds | int | `10` | Period seconds for startupProbe |
| postgresql.startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| postgresql.startupProbe.timeoutSeconds | int | `1` | Timeout seconds for startupProbe |
| postgresql.terminationGracePeriodSeconds | string | `""` | Seconds PostgreSQL pod needs to terminate gracefully |
| postgresql.updateStrategy.rollingUpdate | object | `{}` | PostgreSQL statefulset rolling update configuration parameters |
| postgresql.updateStrategy.type | string | `"RollingUpdate"` | PostgreSQL statefulset update strategy type |
| readinessProbe.enabled | bool | `true` | Enable readinessProbe on Wandertag App Backend containers |
| readinessProbe.failureThreshold | int | `3` | Failure threshold for readinessProbe |
| readinessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for readinessProbe |
| readinessProbe.periodSeconds | int | `10` | Period seconds for readinessProbe |
| readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| readinessProbe.timeoutSeconds | int | `5` | Timeout seconds for readinessProbe |
| replicaCount | int | `1` | Number of Wandertag App Backend Webserver replicas to deploy |
| resources.limits.memory | string | `"1Gi"` | The memory limits for the Wandertag App Backend containers |
| resources.requests.cpu | string | `"125m"` | The requested cpu for the Wandertag App Backend containers |
| resources.requests.memory | string | `"500Mi"` | The requested memory for the Wandertag App Backend containers |
| service.port | int | `80` | Wandertag App Backend service port |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for the Wandertag App Backend application If set to false, the default serviceAccount is used. |
| serviceAccount.name | string | wandertag-app.fullname | The name of the service account to use. |
| startupProbe.enabled | bool | `true` | Enable startupProbe on Wandertag App Backend containers |
| startupProbe.failureThreshold | int | `20` | Failure threshold for startupProbe |
| startupProbe.initialDelaySeconds | int | `10` | Initial delay seconds for startupProbe |
| startupProbe.periodSeconds | int | `10` | Period seconds for startupProbe |
| startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| strategy.rollingUpdate.maxSurge | int | `1` | Maximum number of Wandertag App Backend Pods that can be created over the desired number of Pods |
| strategy.rollingUpdate.maxUnavailable | int | `0` | Maximum number of Wandertag App Backend Pods that can be unavailable during the update process |
| strategy.type | string | `"RollingUpdate"` | The strategy used to replace old Pods |

## License

This project is licensed under [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0), see [LICENSE](LICENSE).

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.14.2](https://github.com/norwoodj/helm-docs/releases/v1.14.2)
