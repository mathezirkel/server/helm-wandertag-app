# Common Chart parameters

# -- String to partially override wandertag-app.fullname
nameOverride: ""
# -- String to fully override wandertag-app.fullname
fullnameOverride: ""

# Webserver

image:
  # -- Wandertag App Backend image repository
  repository: registry.gitlab.com/mathezirkel/content/wandertag-app
  # -- Wandertag App Backend image tag
  tag: "main"
  # -- Wandertag App Backend image pull policy
  pullPolicy: Always

# -- Number of Wandertag App Backend Webserver replicas to deploy
replicaCount: 1

strategy:
  rollingUpdate:
    # -- Maximum number of Wandertag App Backend Pods that can be created over the desired number of Pods
    maxSurge: 1
    # -- Maximum number of Wandertag App Backend Pods that can be unavailable during the update process
    maxUnavailable: 0
  # -- The strategy used to replace old Pods
  type: RollingUpdate

startupProbe:
  # -- Enable startupProbe on Wandertag App Backend containers
  enabled: true
  # -- Initial delay seconds for startupProbe
  initialDelaySeconds: 10
  # -- Period seconds for startupProbe
  periodSeconds: 10
  # -- Timeout seconds for startupProbe
  timeoutSeconds: 5
  # -- Failure threshold for startupProbe
  failureThreshold: 20
  # -- Success threshold for startupProbe
  successThreshold: 1

readinessProbe:
  # -- Enable readinessProbe on Wandertag App Backend containers
  enabled: true
  # -- Initial delay seconds for readinessProbe
  initialDelaySeconds: 10
  # -- Period seconds for readinessProbe
  periodSeconds: 10
  # -- Timeout seconds for readinessProbe
  timeoutSeconds: 5
  # -- Failure threshold for readinessProbe
  failureThreshold: 3
  # -- Success threshold for readinessProbe
  successThreshold: 1

livenessProbe:
  # -- Enable livenessProbe on Wandertag App Backend containers
  enabled: true
  # -- Initial delay seconds for livenessProbe
  initialDelaySeconds: 10
  # -- Period seconds for livenessProbe
  periodSeconds: 10
  # -- Timeout seconds for livenessProbe
  timeoutSeconds: 5
  # -- Failure threshold for livenessProbe
  failureThreshold: 3
  # -- Success threshold for livenessProbe
  successThreshold: 1

resources:
  limits:
    # -- The memory limits for the Wandertag App Backend containers
    memory: 1Gi
  requests:
    # -- The requested cpu for the Wandertag App Backend containers
    cpu: 125m
    # -- The requested memory for the Wandertag App Backend containers
    memory: 500Mi

service:
  # -- Wandertag App Backend service port
  port: 80

config:
  # -- The webserver interface to listen on
  host: 0.0.0.0
  # -- The containerPort of the webserver
  port: 8888
  # -- Enables the upload of data points
  uploadActive: false

# Postgresql

postgresql:

  # -- String to partially override postgresql.fullname
  nameOverride: ""
  # -- String to fully override postgresql.fullname
  fullnameOverride: ""

  image:
    # -- PostgreSQL image repository
    repository: postgres
    # -- PostgreSQL image tag
    # @default -- postgresql.Chart.appVersion
    tag: "16.3-alpine"
    # -- PostgreSQL image pull policy
    pullPolicy: IfNotPresent

  # -- Number of PostgreSQL replicas to deploy
  replicaCount: 1
  # -- Pod management policy for the PostgreSQL statefulset
  podManagementPolicy: OrderedReady

  updateStrategy:
    # -- PostgreSQL statefulset update strategy type
    type: RollingUpdate
    # -- PostgreSQL statefulset rolling update configuration parameters
    rollingUpdate: {}

  # -- Seconds PostgreSQL pod needs to terminate gracefully
  terminationGracePeriodSeconds: ""

  startupProbe:
    # -- Enable startupProbe on PostgreSQL containers
    enabled: true
    # -- Initial delay seconds for startupProbe
    initialDelaySeconds: 30
    # -- Period seconds for startupProbe
    periodSeconds: 10
    # -- Timeout seconds for startupProbe
    timeoutSeconds: 1
    # -- Failure threshold for startupProbe
    failureThreshold: 15
    # -- Success threshold for startupProbe
    successThreshold: 1

  readinessProbe:
    # -- Enable readinessProbe on PostgreSQL containers
    enabled: true
    # -- Initial delay seconds for readinessProbe
    initialDelaySeconds: 5
    # -- Period seconds for readinessProbe
    periodSeconds: 10
    # -- Timeout seconds for readinessProbe
    timeoutSeconds: 5
    # -- Failure threshold for readinessProbe
    failureThreshold: 6
    # -- Success threshold for readinessProbe
    successThreshold: 1

  livenessProbe:
    # -- Enable livenessProbe on PostgreSQL containers
    enabled: true
    # -- Initial delay seconds for livenessProbe
    initialDelaySeconds: 30
    # -- Period seconds for livenessProbe
    periodSeconds: 10
    # -- Timeout seconds for livenessProbe
    timeoutSeconds: 5
    # -- Failure threshold for livenessProbe
    failureThreshold: 6
    # -- Success threshold for livenessProbe
    successThreshold: 1

  resources:
    requests:
      # -- The requested cpu for the PostgreSQL containers
      cpu: 100m
      # -- The requested memory for the PostgreSQL containers
      memory: 256Mi
    limits:
      # -- The memory limits for the PostgreSQL containers
      memory: 512Mi

  # Admin credentials

  existingSecret:
    # -- Name of an existing secret containing the admin user credentials
    name: database-credentials
    # -- Key containing the admin user password
    passwordKey: password

  # Persistence

  persistence:
    # -- The path the volume will be mounted inside the container
    mountPath: /var/lib/postgresql/data
    # -- The subdirectory of the volume to mount
    subPath: ""
    # -- PVC Access Mode for PostgreSQL volume
    accessModes:
      - ReadWriteOnce
    # -- PVC StorageClass for PostgreSQL data volume
    storageClassName: retain-local-path
    # -- PVC Storage Request for PostgreSQL volume
    size: 10Gi

  # Service

  service:
    # -- PostgreSQL service port
    port: 5432

  # Service account

  serviceAccount:
    # -- Enable creation of ServiceAccount for PostgreSQL pod
    # If set to false, the default ServiceAccount is used.
    create: true
    # -- The name of the service account to use.
    # @default -- postgresql.fullname
    name: ""

dataSecret:
  existingSecret:
    name: wandertag-app-credentials
    key: data-secret

# Service Account

serviceAccount:
  # -- Enable creation of ServiceAccount for the Wandertag App Backend application
  # If set to false, the default serviceAccount is used.
  create: true
  # -- The name of the service account to use.
  # @default -- wandertag-app.fullname
  name: ""

# Ingress

ingress:
  # -- Enable the ingress
  enabled: true
  # -- List of traefik entrypoints to listen
  entryPoints:
    - websecure
  # -- Hostname the Wandertag App Backend should be available on
  host: wandertag-app-backend.mathezirkel-augsburg.de
  # -- List of additional middlewares to apply on the ingress
  # @section -- Ingress
  additionalMiddlewares:
    - name: security-headers
      namespace: kube-public
  tls:
    # -- Name of the certResolver configured via traefik
    certResolver: lets-encrypt
